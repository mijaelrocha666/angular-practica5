import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-condicional',
  templateUrl: './condicional.component.html',
  styleUrls: ['./condicional.component.css']
})
export class CondicionalComponent implements OnInit {
 
  persona: any = {
    nombreC: 'Jose Mijael Rocha Vera',
    direccion: 'Av. Blanco Galindo #666' 
  }

  loading: boolean = false;
   
  constructor() { }

  ngOnInit(): void {
  }

  ejecutar(): void{
    this.loading = true;

    setTimeout(() => {
      this.loading = false;
    }, 5000);
  }

}
